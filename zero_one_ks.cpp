#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define fo(i,b,n) for(int i=(b); i<(n) ; i++)
#define xx first
#define yy second
#define pb push_back
#define sz(n) int(n.size())

#define pii pair
#define MP make_pair

#define tp tuple
#define MT make_tuple
#define gt(x,y) get<x>(y)

#define read(x) scanf("%d",&x)
#define read2(x,y) scanf("%d%d",&x,&y)
#define readl(x) scanf("%lld",&x)
#define readl2(x,y) scanf("%lld%lld",&x,&y)
#define readd(x) scanf("%lf",&x)
#define readd2(x,y) scanf("%lf%lf",&x,&y)

#define pr(n) printf("%d",n)
#define prn(n) printf("%d\n",n)
#define prl(n) printf("%lld",n)
#define prln(n) printf("%lld\n",n)
#define prd(x) printf("%lf",x)
#define prdn(x) printf("%lf\n",x)

#define TC(n) printf("Case %d: ",n)
/**********************************/

const int N=1005;
int w[N],v[N];
ll dp[N][N];

int main()
{
    int n,cap;
    read2(n,cap);
    fo(i,1,n+1)
    {
        read2(w[i],v[i]);
    }
    fo(i,0,n)dp[0][i]=0;
    fo(i,1,n+1)
    {
        fo(j,1,cap+1)
        {
            dp[i][j]=dp[i-1][j];
            if( j-w[i] >=0 )dp[i][j]=max(dp[i][j],v[i]+dp[i-1][j-w[i]]);
        }
    }
    
//    fo(i,0,n+1){
//        fo(j,0,cap+1){
//            cout<<dp[i][j]<<" ";
//        }
//        cout<<endl;
//    }

    prln(dp[n][cap]);
    return 0;
}
















