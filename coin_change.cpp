#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define fo(i,b,n) for(int i=(b); i<(n) ; i++)
#define xx first
#define yy second
#define pb push_back
#define sz(n) int(n.size())

#define pii pair
#define MP make_pair

#define tp tuple
#define MT make_tuple
#define gt(x,y) get<x>(y)

#define read(x) scanf("%d",&x)
#define read2(x,y) scanf("%d%d",&x,&y)
#define readl(x) scanf("%lld",&x)
#define readl2(x,y) scanf("%lld%lld",&x,&y)
#define readd(x) scanf("%lf",&x)
#define readd2(x,y) scanf("%lf%lf",&x,&y)

#define pr(n) printf("%d",n)
#define prn(n) printf("%d\n",n)
#define prl(n) printf("%lld",n)
#define prln(n) printf("%lld\n",n)
#define prd(x) printf("%lf",x)
#define prdn(x) printf("%lf\n",x)

#define TC(n) printf("Case %d: ",n)
/**********************************/

const int N=1005;
int coins[N];
int make;
ll dp[N][N];

int main()
{
    int n;
    read2(n,make);
    fo(i,1,n+1)read(coins[i]);
    fo(i,0,n+1)dp[i][0]=1;
    fo(i,0,make)dp[0][i]=0;

    fo(i,1,n+1){
        fo(j,1,make+1){
            dp[i][j]=dp[i-1][j];
            if( j-coins[i] >=0 )dp[i][j]+=dp[i][j-coins[i]];
        }
    }

    prln(dp[n][make]);
    return 0;
}

















