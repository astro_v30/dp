#include<bits/stdc++.h>
//astro_lion 
using namespace std;

#define ll long long int
#define fo(i,b,n) for(int i=(b); i<(n) ; i++)
#define xx first
#define yy second
#define pb push_back
#define sz(n) int(n.size())
#define pii pair
#define MP make_pair

#define read(x) scanf("%d",&x)
#define read2(x,y) scanf("%d%d",&x,&y)
#define readl(x) scanf("%lld",&x)
#define readl2(x,y) scanf("%lld%lld",&x,&y)
#define readd(x) scanf("%lf",&x)
#define readd2(x,y) scanf("%lf%lf",&x,&y)

#define pr(n) printf("%d",n)
#define prn(n) printf("%d\n",n)
#define prl(n) printf("%lld",n)
#define prln(n) printf("%lld\n",n)
#define prd(x) printf("%lf",x)
#define prdn(x) printf("%lf\n",x)

#define TC(n) printf("Case %d: ",n)

string s;
ll dp[12][2][90];

ll a,b,m,str_len;

void to_str(ll n){
    s="";
    while(n){
        s+=(n%10)+'0';
        n/=10;
    }
    reverse(s.begin(),s.end());
}

ll solve(int idx,int flag,int mod){

    if(idx==str_len) return (ll) mod==0;

    ll &ret=dp[idx][flag][mod];
    if(ret ^ -1) return ret;

    int lim=9;
    if(flag)lim=s[idx]-'0';

    ll cnt=0;
    fo(i,0,lim+1){
        if(i<s[idx]-'0') cnt+=solve(idx+1,0, (mod+i)%m );
        else cnt+=solve(idx+1,flag, (mod+i)%m);
    }

    ret=cnt;
    return ret;
}

int main()
{
    cin>>a>>b>>m;

    memset(dp,-1,sizeof dp);

    if(a)a--;
    to_str(a);
    //cout<<s<<endl;
    str_len=sz(s);
    ll one=solve(0,1,0);

    to_str(b);
    //cout<<s<<endl;

    memset(dp,-1,sizeof dp);

    str_len=sz(s);
    ll two=solve(0,1,0);
    cout<<(two-one)<<endl;
    return 0;

}















