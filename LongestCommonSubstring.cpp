#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define fo(i,b,n) for(int i=(b); i<(n) ; i++)
#define xx first
#define yy second
#define pb push_back
#define sz(n) int(n.size())

#define pii pair
#define MP make_pair

#define tp tuple
#define MT make_tuple
#define gt(x,y) get<x>(y)

#define read(x) scanf("%d",&x)
#define read2(x,y) scanf("%d%d",&x,&y)
#define readl(x) scanf("%lld",&x)
#define readl2(x,y) scanf("%lld%lld",&x,&y)
#define readd(x) scanf("%lf",&x)
#define readd2(x,y) scanf("%lf%lf",&x,&y)

#define pr(n) printf("%d",n)
#define prn(n) printf("%d\n",n)
#define prl(n) printf("%lld",n)
#define prln(n) printf("%lld\n",n)
#define prd(x) printf("%lf",x)
#define prdn(x) printf("%lf\n",x)

#define TC(n) printf("Case %d: ",n)
/**********************************/
//Longest common substriong ...

const int N=1005;
string  ans[N][N];
int dp[N][N];

int main()
{

    string s1,s2;
    cin>>s1>>s2;
    int len1=sz(s1),len2=sz(s2);

    int mx=-1;
    string a;
    fo(i,1,len1+1){
        fo(j,1,len2+1)
        {
            if(s1[i-1] ^ s2[j-1] ){
                dp[i][j]=0;
            }
            else{
                dp[i][j]=1+dp[i-1][j-1];
                ans[i][j]+=ans[i-1][j-1]+s1[i-1];
                if(dp[i][j] > mx ){
                    mx=dp[i][j];
                    a=ans[i][j];
                }
            }
        }
    }

    fo(i,0,len1+1){
        fo(j,0,len2+1)cout<<dp[i][j]<<" ";
        puts("");
    }
    puts("");

    fo(i,0,len1+1){
        fo(j,0,len2+1)cout<<ans[i][j]<<" ";
        puts("");
    }


    cout<<mx<<endl;
    cout<<a<<endl;



    return 0;
}
















